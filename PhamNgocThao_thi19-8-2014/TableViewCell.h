//
//  TableViewCell.h
//  PhamNgocThao_thi19-8-2014
//
//  Created by MAC on 8/19/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
