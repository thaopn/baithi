//
//  SecondViewController.h
//  PhamNgocThao_thi19-8-2014
//
//  Created by MAC on 8/19/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *strUrl;
@end
