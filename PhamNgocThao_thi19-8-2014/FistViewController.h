//
//  FistViewController.h
//  PhamNgocThao_thi19-8-2014
//
//  Created by MAC on 8/19/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FistViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg;

- (IBAction)seg:(id)sender;

@end
