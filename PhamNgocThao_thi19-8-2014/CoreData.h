//
//  CoreData.h
//  PhamNgocThao_thi19-8-2014
//
//  Created by MAC on 8/19/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CoreData : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * name;

@end
