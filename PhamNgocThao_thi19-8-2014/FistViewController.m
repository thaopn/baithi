//
//  FistViewController.m
//  PhamNgocThao_thi19-8-2014
//
//  Created by MAC on 8/19/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "FistViewController.h"
#import "TableViewCell.h"
#import "SecondViewController.h"
#import "AppDelegate.h"
#import "CoreData.h"

@interface FistViewController ()

@end

@implementation FistViewController{
    NSArray *arrObject;
    NSMutableArray *arrImg;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self getDataFromService];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
}

-(void)getDataFromService{
    arrImg = nil;
    arrImg = [[NSMutableArray alloc]initWithCapacity:10];
    
    NSString *strUrl = @"http://thanhliem.webfactional.com/kidschannel/Api/getSongListPage/494527/0/10";
    NSURL *url = [[NSURL alloc]initWithString:strUrl];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        arrObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        NSLog(@"%@", arrObject);
        for (NSDictionary *dict in arrObject) {
            
        
        NSString *strImgUrl = [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",dict[@"youtubeid"]];
        
        NSURL *url = [[NSURL alloc]initWithString:strImgUrl];
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//            [arrImg addObject:data];
//            //        NSLog(@"%@", arrObject);
//            
//        }];
            NSURLResponse *re=nil;
           NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&re error:nil];
            [arrImg addObject:data];
        [_tableView reloadData];
        }
        [_tableView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    return arrObject.count;
  
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell"];
    if(_seg.selectedSegmentIndex == 0)
    {
        NSDictionary *dict = arrObject[indexPath.row];
        if (arrImg.count>0)
            cell.img.image = [UIImage imageWithData:arrImg[indexPath.row]];
        cell.lblName.text = dict[@"names"];
        
    }
    else if(_seg.selectedSegmentIndex == 1)
    {
        CoreData *core = [arrObject objectAtIndex:indexPath.row];
        cell.lblName.text = core.name;
        cell.img.image = [UIImage imageWithData:core.image];
    }
    
    cell.tag = indexPath.row;
    return cell;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//        cell.img.image = [UIImage imageWithData:arrImg[indexPath.row]];
//    
//}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"PushFromFistView"]) {
        TableViewCell *cell = sender;
        int index = cell.tag;
        NSString *str;
        if(_seg.selectedSegmentIndex==0)
        {
            NSDictionary *dict = arrObject[index];
            str = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@",dict[@"youtubeid"]];
        
            AppDelegate *deleget = [UIApplication sharedApplication].delegate;
            CoreData *core = [NSEntityDescription insertNewObjectForEntityForName:@"CoreData" inManagedObjectContext:deleget.managedObjectContext];
            //set thuoc tinh
            core.id = dict[@"youtubeid"];
            core.image = arrImg[index];
            core.name = dict[@"names"];
            [deleget.managedObjectContext save:nil];
        }
        if (_seg.selectedSegmentIndex==1) {
            CoreData *core = [arrObject objectAtIndex:index];
            str = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@",core.id];
        }
        SecondViewController *view = [segue destinationViewController];
        view.strUrl = str;
    }
}

-(void)loadFromServer{
    [self getDataFromService];
}

-(void)loadFromCoreData{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CoreData" inManagedObjectContext:delegate.managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc]init];
    [fetch setEntity:entity];
    arrObject = [delegate.managedObjectContext executeFetchRequest:fetch error:nil];
    [_tableView reloadData];
   
}

- (IBAction)seg:(id)sender {
    switch (_seg.selectedSegmentIndex) {
        case 0:
            [self loadFromServer];
            break;
            
        case 1:
            [self loadFromCoreData];
            break;
            
        default:
            break;
    }
}
@end
